SET GLOBAL event_scheduler = ON;

delimiter |

CREATE EVENT CONVERT_DREAM_TOKEN_TO_USD
  ON SCHEDULE
    EVERY 1 DAY
    STARTS '2022-03-10 00:00:00' ON COMPLETION PRESERVE ENABLE
  DO
  BEGIN
    SELECT one_dream_token_to_usd INTO @EXCHANGE_RATE FROM dreamland.dream_token_exchange_rate order by id DESC LIMIT 1;
    UPDATE user_tokens SET dream_token_amount_in_usd = @EXCHANGE_RATE*dream_token_amount, modified_at = utc_timestamp() where dream_token_amount_in_usd is null and created_at < CURDATE();
  END |

delimiter ;