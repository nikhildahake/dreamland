package com.dreamland.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dreamland.helpers.UserStats;
import com.dreamland.helpers.UserTokenView;
import com.dreamland.models.User;
import com.dreamland.services.DreamlandService;

/**
 * @author nikhildahake
 */
@Controller
public class DreamlandController {
	
	@Autowired
	private DreamlandService dreamlandService;
	
	@RequestMapping({"/"})
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "/api/test", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getTestResponse() {
		return ResponseEntity.ok("Test response.");
	}
	
	@RequestMapping(value = "/api/user", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> createUser(HttpServletRequest request) {
		List<String> errors = new ArrayList<String>();
		String firstName = request.getParameter("firstName");
		if (!isValidNameString(firstName)) {
			errors.add("First name must not be null and should have length > 0");
		}
		
		String lastName = request.getParameter("lastName");
		if (!isValidNameString(lastName)) {
			errors.add("Last name must not be null and should have length > 0");
		}
		
		String email = request.getParameter("email");
		if (!isValidEmail(email)) {
			errors.add("Email should have valid format of type: xyz@google.com");
		}
		
		String password = request.getParameter("password");
		if (!isValidPassword(password)) {
			errors.add("Password must not be null and should have length > 0");
		}
		
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		User user = dreamlandService.createUser(firstName, lastName, email, password);
		return ResponseEntity.ok("User created with id: " + user.getId());
	}
	
	@RequestMapping(value = "/api/user/{id}/token/{token_amount}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?>addToken(@PathVariable String id, @PathVariable String token_amount) {
		List<String> errors = new ArrayList<String>();
		long userId = 0;
		float tokenAmount = 0;
		
		try {
			userId = Long.parseLong(id);
		}
		catch (Exception e) {
			errors.add("User id should be a valid integer");
		}
		
		try {
			tokenAmount = Float.parseFloat(token_amount);
		}
		catch (Exception e) {
			errors.add("Token amount should be a valid decimal");
		}
		
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		errors = dreamlandService.addToken(userId, tokenAmount);
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		return ResponseEntity.ok(token_amount + " has been added to user id: " + id);
	}
	
	@RequestMapping(value = "/api/user/{id}/tokens/today", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?>getTokensForToday(@PathVariable String id) {
		List<String> errors = new ArrayList<String>();
		long userId = 0;
		try {
			userId = Long.parseLong(id);
		}
		catch (Exception e) {
			errors.add("User id should be a valid integer");
		}
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		HashMap<String, Object> hm = dreamlandService.getDreamlandTokensForToday(userId);
		if (hm.containsKey("errors")) {
			 errors = (List<String>)hm.get("errors");
			 return ResponseEntity.badRequest().body(errors);
		}
		else {
			List<UserTokenView> views = (List<UserTokenView>)hm.get("views");
			return ResponseEntity.ok(views);
		}
	}
	
	@RequestMapping(value = "/api/user/{id}/usd_tokens/till/EOD_YESTERDAY", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getUSDAmountsTillEODYesterday(@PathVariable String id) {
		List<String> errors = new ArrayList<String>();
		long userId = 0;
		try {
			userId = Long.parseLong(id);
		}
		catch (Exception e) {
			errors.add("User id should be a valid integer");
		}
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		
		HashMap<String, Object> hm = dreamlandService.getUSDAmountsTillEODYesterday(userId);
		if (hm.containsKey("errors")) {
			 errors = (List<String>)hm.get("errors");
			 return ResponseEntity.badRequest().body(errors);
		}
		else {
			List<UserTokenView> views = (List<UserTokenView>)hm.get("views");
			return ResponseEntity.ok(views);
		}
	}
	
	@RequestMapping(value = "/api/user/{id}/stats", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getUserStats(@PathVariable String id) {
		List<String> errors = new ArrayList<String>();
		long userId = 0;
		try {
			userId = Long.parseLong(id);
		}
		catch (Exception e) {
			errors.add("User id should be a valid integer");
		}
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		HashMap<String, Object> hm = dreamlandService.getUserStats(userId);
		if (hm.containsKey("errors")) {
			 errors = (List<String>)hm.get("errors");
			 return ResponseEntity.badRequest().body(errors);
		}
		else {
			UserStats userStats = (UserStats)hm.get("userstats");
			return ResponseEntity.ok(userStats);
		}
	}
	
	@RequestMapping(value = "/api/user/{id}/tokens/for/{date}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getUserStats(@PathVariable String id, @PathVariable String date) {
		List<String> errors = new ArrayList<String>();
		long userId = 0;
		try {
			userId = Long.parseLong(id);
		}
		catch (Exception e) {
			errors.add("User id should be a valid integer");
		}
		if (errors.size() > 0) {
			return ResponseEntity.badRequest().body(errors);
		}
		
		HashMap<String, Object> hm = dreamlandService.getUserTokensForDate(userId, date);
		if (hm.containsKey("errors")) {
			 errors = (List<String>)hm.get("errors");
			 return ResponseEntity.badRequest().body(errors);
		}
		else {
			return ResponseEntity.ok(hm);
		}
	}
	
	private boolean isValidNameString(String nameString) {
		if (nameString == null) {
			return false;
		}
		
		if (nameString.trim().length() == 0) {
			return false;
		}
		
		return true;
	}
	
	private boolean isValidEmail(String email) {
		if (email == null) {
			return false;
		}
		
		if (email.trim().length() == 0) {
			return false;
		}
		
		return true;
	}
	
	private boolean isValidPassword(String password) {
		if (password == null) {
			return false;
		}
		
		if (password.trim().length() == 0) {
			return false;
		}
		
		return true;
	}
}