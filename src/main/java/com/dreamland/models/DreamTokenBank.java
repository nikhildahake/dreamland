package com.dreamland.models;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "dream_token_bank")
@Data
@NoArgsConstructor
public class DreamTokenBank {
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "dream_token_bank_balance")
	private Float dreamTokenBankBalance;
	
	@Column(name = "created_at", nullable = false)
    private Instant createdAt;
    
    @Column(name = "modified_at", nullable = false)
    private Instant modifiedAt;
    
    @PrePersist
    protected void prePersist() {
	    if (this.createdAt == null) {
	    	createdAt = Instant.now();
	    }
	    
        if (this.modifiedAt == null) {
        	modifiedAt = Instant.now();
        }
    }

    @PreUpdate
    protected void preUpdate() {
        this.modifiedAt = Instant.now();
    }

    @PreRemove
    protected void preRemove() {
        this.modifiedAt = Instant.now();
    }
}