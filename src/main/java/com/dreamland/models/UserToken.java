package com.dreamland.models;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user_tokens")
@Data
@NoArgsConstructor
public class UserToken {
	
	public UserToken(long userId, float dreamTokenAmount) {
		this.userId = userId;
		this.dreamTokenAmount = dreamTokenAmount;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "user_id")
	private Long userId;
	
	@Column(name = "dream_token_amount")
	private float dreamTokenAmount;
	
	@Column(name = "dream_token_amount_in_usd")
	private Float dreamTokenAmountInUSD = null;
	

	@Column(name = "created_at", nullable = false)
    private Instant createdAt;
    
    @Column(name = "modified_at", nullable = false)
    private Instant modifiedAt;
    
    @PrePersist
    protected void prePersist() {
	    if (this.createdAt == null) {
	    	createdAt = Instant.now();
	    }
	    
        if (this.modifiedAt == null) {
        	modifiedAt = Instant.now();
        }
    }

    @PreUpdate
    protected void preUpdate() {
        this.modifiedAt = Instant.now();
    }

    @PreRemove
    protected void preRemove() {
        this.modifiedAt = Instant.now();
    }
}