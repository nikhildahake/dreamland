package com.dreamland.models;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User {
	
	public User(String firstName, String lastName, String email, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;

	@Column(name = "created_at", nullable = false)
    private Instant createdAt;
    
    @Column(name = "modified_at", nullable = false)
    private Instant modifiedAt;
    
    @PrePersist
    protected void prePersist() {
	    if (this.createdAt == null) {
	    	createdAt = Instant.now();
	    }
	    
        if (this.modifiedAt == null) {
        	modifiedAt = Instant.now();
        }
    }

    @PreUpdate
    protected void preUpdate() {
        this.modifiedAt = Instant.now();
    }

    @PreRemove
    protected void preRemove() {
        this.modifiedAt = Instant.now();
    }
}