package com.dreamland.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dreamland.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findById(long userId);
	User findByEmail(String email);
}