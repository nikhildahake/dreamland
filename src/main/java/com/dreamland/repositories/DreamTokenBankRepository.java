package com.dreamland.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dreamland.models.DreamTokenBank;

public interface DreamTokenBankRepository extends JpaRepository<DreamTokenBank, Long> { 
	DreamTokenBank findById(long userId);
}
