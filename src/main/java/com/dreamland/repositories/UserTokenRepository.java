package com.dreamland.repositories;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dreamland.models.UserToken;

public interface UserTokenRepository extends JpaRepository<UserToken, Long> { 
	@Query("Select SUM(ut.dreamTokenAmount) from UserToken ut where ut.createdAt >= CURDATE() and ut.userId = ?1")
	Float getSumOfTokensEarnedToday(long userId);
	
	@Query("Select ut from UserToken ut where ut.createdAt > ?2 and ut.createdAt < ?3 and ut.userId = ?1")
	List<UserToken> getUserTokensForDate(long userId, Instant startOfDay, Instant startOfNextDay);
	
	@Query("Select SUM(ut.dreamTokenAmountInUSD) from UserToken ut where ut.createdAt > ?2 and ut.createdAt < ?3 and ut.userId = ?1")
	Float getUSDAmountForDate(long userId, Instant startOfDay, Instant startOfNextDay);
	
	@Query("Select ut from UserToken ut where ut.createdAt >= CURDATE() And ut.userId = ?1")
	List<UserToken> getTokensForToday(long userId);

	@Query("Select ut from UserToken ut where ut.createdAt < CURDATE() And ut.userId = ?1")
	List<UserToken> getUSDAmountsTillEODYesterday(long userId);
	
	@Query("Select SUM(ut.dreamTokenAmountInUSD) from UserToken ut where ut.userId = ?1")
	Float getTotalAccountValueInUSD(long userId);
}
