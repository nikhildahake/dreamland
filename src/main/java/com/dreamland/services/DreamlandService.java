package com.dreamland.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dreamland.helpers.UserStats;
import com.dreamland.helpers.UserTokenView;
import com.dreamland.models.DreamTokenBank;
import com.dreamland.models.User;
import com.dreamland.models.UserToken;
import com.dreamland.repositories.DreamTokenBankRepository;
import com.dreamland.repositories.UserRepository;
import com.dreamland.repositories.UserTokenRepository;

@Service
public class DreamlandService {
			
	final int DAILY_MAX_EARNINGS = 5;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserTokenRepository userTokenRepository;
	
	@Autowired
	private DreamTokenBankRepository dreamTokenBankRepository;
	
	@Transactional
	public User createUser(String firstName, String lastName, String email, String password) {
		User user = new User(firstName, lastName, email, password);
		User savedUser = userRepository.save(user);
		return savedUser;
	}
	
	@Transactional
	public List<String> addToken(long userId, float tokenAmount) {
		List<String> errors = new ArrayList<String>();
		DreamTokenBank dreamTokenBank = dreamTokenBankRepository.findById(1L);
		
		
		User user = userRepository.findById(userId);
		if (user == null) {
			errors.add("User with user id: " + userId + " does not exist.");
			return errors;
		}
		
		Float sumTotal = userTokenRepository.getSumOfTokensEarnedToday(userId);
		sumTotal = sumTotal == null? 0 : sumTotal;
		if ((sumTotal + tokenAmount <= DAILY_MAX_EARNINGS) && (dreamTokenBank.getDreamTokenBankBalance() >= tokenAmount)) {
			UserToken userToken = new UserToken(userId, tokenAmount);
			dreamTokenBank.setDreamTokenBankBalance(dreamTokenBank.getDreamTokenBankBalance() - tokenAmount);
			dreamTokenBankRepository.save(dreamTokenBank);
			userTokenRepository.save(userToken);
		}
		else {
			errors.add("If you add token with amount: " + tokenAmount + " you will be over the daily limit. "
					+ "Your current total for the day is: " + sumTotal + " . Daily max limit is: " + DAILY_MAX_EARNINGS);
		}
		return errors;
	}
	
	@Transactional
	public HashMap<String, Object> getDreamlandTokensForToday(long userId) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		
		User user = userRepository.findById(userId);
		if (user == null) {
			List<String> errors = new ArrayList<String>();
			errors.add("User with id: " + userId + " does not exist.");
			hm.put("errors", errors);
			return hm;
		}
		
		List<UserToken> userTokens = userTokenRepository.getTokensForToday(userId);
		List<UserTokenView> views = new ArrayList<UserTokenView>();
		
		for(UserToken ut: userTokens) {
			views.add(new UserTokenView(ut.getUserId(), ut.getDreamTokenAmount(), ut.getCreatedAt()));
		}
		hm.put("views", views);
		return hm;
	}
	
	@Transactional
	public HashMap<String, Object> getUSDAmountsTillEODYesterday(long userId) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		
		User user = userRepository.findById(userId);
		if (user == null) {
			List<String> errors = new ArrayList<String>();
			errors.add("User with id: " + userId + " does not exist.");
			hm.put("errors", errors);
			return hm;
		}
		
		List<UserToken> userTokens = userTokenRepository.getUSDAmountsTillEODYesterday(userId);
		List<UserTokenView> views = new ArrayList<UserTokenView>();
		
		for(UserToken ut: userTokens) {
			views.add(new UserTokenView(ut.getUserId(), ut.getDreamTokenAmountInUSD(), ut.getCreatedAt()));
		}
		hm.put("views", views);
		return hm;
	}
	
	@Transactional
	public HashMap<String, Object> getUserStats(long userId) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		User user = userRepository.findById(userId);
		if (user == null) {
			List<String> errors = new ArrayList<String>();
			errors.add("User with id: " + userId + " does not exist.");
			hm.put("errors", errors);
			return hm;
		}
		
		Float sum_of_dream_tokens_won_today = userTokenRepository.getSumOfTokensEarnedToday(userId);
		Float total_account_usd_value = userTokenRepository.getTotalAccountValueInUSD(userId);
		UserStats userStats = new UserStats(userId, sum_of_dream_tokens_won_today, total_account_usd_value);
		hm.put("userstats", userStats);
		return hm;
	}
	
	@Transactional
	public HashMap<String, Object> getUserTokensForDate(long userId, String dateString) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		User user = userRepository.findById(userId);
		if (user == null) {
			List<String> errors = new ArrayList<String>();
			errors.add("User with id: " + userId + " does not exist.");
			hm.put("errors", errors);
			return hm;
		}
		
		LocalDate date = LocalDate.parse(dateString);
		Instant startOfDay = date.atStartOfDay(ZoneId.of("UTC")).toInstant();
		Instant startOfNextDay = startOfDay.plus(1, ChronoUnit.DAYS);
		
		List<UserToken> userTokens = userTokenRepository.getUserTokensForDate(userId, startOfDay, startOfNextDay);
		Float usdAmount = userTokenRepository.getUSDAmountForDate(userId, startOfDay, startOfNextDay);
		List<UserTokenView> views = new ArrayList<UserTokenView>();
		
		for(UserToken ut: userTokens) {
			views.add(new UserTokenView(ut.getUserId(), ut.getDreamTokenAmount(), ut.getCreatedAt()));
		}
		hm.put("DreamTokens", views);
		hm.put("USDAmount", usdAmount);
		
		return hm;
	}
}