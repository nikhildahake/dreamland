package com.dreamland.helpers;

import java.time.Instant;

import lombok.Data;

@Data
public class UserTokenView {
	public UserTokenView(long userId, Float amount, Instant createdAt) {
		this.userId = userId;
		this.amount = amount;
		this.createdAt = createdAt;
	}
	
	long userId;
	Float amount;
	Instant createdAt;
}
