package com.dreamland.helpers;

import lombok.Data;

@Data
public class UserStats {
	long userId;
	Float sum_of_dream_tokens_won_today;
	Float total_account_usd_value;
	
	public UserStats(long userId, Float sum_of_dream_tokens_won_today, Float total_account_usd_value) {
		this.userId = userId;
		this.sum_of_dream_tokens_won_today = sum_of_dream_tokens_won_today;
		this.total_account_usd_value = total_account_usd_value;
	}
}
